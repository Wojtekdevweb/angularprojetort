const Sequelize = require('sequelize');

sequelize = new Sequelize('twitter', 'wojtek', 'wojtek123', {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

module.exports = sequelize;