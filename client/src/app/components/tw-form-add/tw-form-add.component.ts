import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Tw } from '../../models/tw';
import { TwService } from '../../services/tw/tw.service';

@Component({
  selector: 'app-tw-form-add',
  templateUrl: './tw-form-add.component.html',
  styleUrls: ['./tw-form-add.component.css']
})
export class TwFormAddComponent implements OnInit {

  @Output() twEvent = new EventEmitter<Tw>();

  tw: Tw = {
    id: null,
    message: '',
    user_id: 1,
    created_at: null,
    updated_at: null,
  };

  constructor(
    private twService: TwService
  ) { }

  ngOnInit(): void {
  }

  saveTw(): void {
    this.twService.saveTw(this.tw).subscribe(tw => {
      this.twEvent.emit(tw);
    });
  }

}
