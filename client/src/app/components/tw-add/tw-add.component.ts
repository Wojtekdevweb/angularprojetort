import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tw-add',
  templateUrl: './tw-add.component.html',
  styleUrls: ['./tw-add.component.css']
})
export class TwAddComponent implements OnInit {

  constructor(
    private location: Location
  ) { }

  ngOnInit(): void {
  }

  receiveTw($event) {
    this.location.back();
  }

}
